angular.module('neibor', ['ionic', 'ngCordova'])

.run(function($ionicPlatform, $rootScope, $ionicLoading) {
	$ionicPlatform.ready(function() {
    	if (window.cordova && window.cordova.plugins.Keyboard) {
      		cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      		cordova.plugins.Keyboard.disableScroll(true);

    	}

	 	if (window.StatusBar) {
      		// org.apache.cordova.statusbar required
      		StatusBar.styleDefault();
    	}
    });
    
    $rootScope.$on('$stateChangeStart', function() {
      $ionicLoading.show({
        template: 'Ladataan...'
      });
    });

    $rootScope.$on('$stateChangeSuccess', function() {
      $ionicLoading.hide();
    });
})

.constant('API', 'http://137.74.45.210:1337') // VPS
//.constant('API', 'http://localhost:1337')
.constant('GOOGLE_API_KEY', 'AIzaSyBPyrk0B28G_J14RK_jXuk41GqBDBi4Vq8')
.constant('CAR_IMAGE_FOLDER', 'http://137.74.45.210/neibor/neibor-back/img/cars')

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
	$stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'pages/menu.html',
    controller: 'neiborCtrl',
    resolve: {
      me: function(Auth) {
        return Auth.verifyAuth();
      }
    }
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'pages/home.html',
        controller: 'homeCtrl'
      }
    },
    resolve: {
      users: function(User) {
        return User.getUsers().then(function(response) {
          return response.data;
        }, function(response) {
          if(response.status == 404) {
            return [];
          }
        });
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'pages/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('app.profile.settings', {
    url: '/settings',
    views: {
      'menuContent@app': {
        templateUrl: 'pages/profile.settings.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('app.user', {
    url: '/user/:id',
    views: {
      'menuContent': {
        templateUrl: 'pages/user.html',
        controller: 'userCtrl'
      }
    },
    resolve: {
      function($stateParams, $state, me) {
        if($stateParams.id == me.data._id) {
          $state.go('app.profile');
        }
      },
      user: function($stateParams, $ionicPopup, $ionicHistory, User) {
        return User.getUserById($stateParams.id).then(function(response) {
          return response.data;
        }, function(response) {
          if(response.status == 404) {
            $ionicPopup.alert({
              title: 'Virhe',
              template: 'Käyttäjää ei löytynyt'
            }).then(function() {
              $ionicHistory.goBack();
            });
          }
        })
      }
    }
  })

  .state('app.car', {
    url: '/car/:id',
    views: {
      'menuContent': {
        templateUrl: 'pages/car.html',
        controller: 'carCtrl'
      }
    },
    resolve: {
      cars: function($stateParams, $ionicPopup, $ionicHistory, Car) {
        return Car.getCarById($stateParams.id).then(function(response) {
          return response.data;
        }, function(response) {
          if(response.status == 404) {
            $ionicPopup.alert({
              title: 'Virhe',
              template: 'Autoa ei löytynyt'
            }).then(function() {
              $ionicHistory.goBack();
            });
          }
        });
      }
    }
  })

  .state('app.car.edit', {
    url: '/edit',
    views: {
      'menuContent@app': {
        templateUrl: 'pages/car.edit.html',
        controller: 'carCtrl'
      }
    }
  })

  .state('app.addCar', {
    url: '/addcar',
    views: {
      'menuContent': {
        templateUrl: 'pages/addCar.html',
        controller: 'addCarCtrl'
      }
    },
    resolve: {
      brands: function(Brand) {
        return Brand.getBrands().then(function(response) {
          return response.data;
        })
      },
      years: function() {
        var years = [];
        var currentYear = new Date().getFullYear();

        for(var i = 1970; i < currentYear; i++) {
          years.push(i);
        }

        return years;
      }
    }
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'pages/login.html',
        controller: 'authCtrl'
      }
    }
  })

  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'pages/register.html',
        controller: 'authCtrl'
      }
    }
  })

  .state('app.logout', {
    url: '/logout',
    views: {
      'menuContent': {
        templateUrl: 'pages/logout.html',
        controller: 'authCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'pages/search.html',
        controller: 'searchCtrl'
      }
    }
  })

  .state('app.faq', {
    url: '/faq',
    views: {
      'menuContent': {
        templateUrl: 'pages/faq.html',
      } 
    }
  })

  .state('app.info', {
    url: '/info',
    views: {
      'menuContent': {
        templateUrl: 'pages/info.html',
      }
    }
  })

	$urlRouterProvider.otherwise('/app/home');

  $httpProvider.interceptors.push('AuthInterceptors');
  $httpProvider.interceptors.push('ErrorInterceptor');
});
