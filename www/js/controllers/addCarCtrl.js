angular.module('neibor').controller('addCarCtrl', function($scope, $state, $ionicHistory, $ionicPopup, $ionicLoading, brands, years, Car, Map, Camera) {
	$scope.car = {};
	$scope.car.imgData = [];
	$scope.brands = brands;
	$scope.models = [];
	$scope.years = years;
	$scope.gears = ['Automaatti', 'Manuaali'];
	$scope.fuels = ['Bensiini', 'Diesel', 'Hybrid', 'Kaasu', 'Sähkö'];

	$scope.getModels = function(brand) {
		$scope.models = brand.models;
	}

	$scope.takePicture = function() {
		if($scope.car.imgData.length < 5) {
			Camera.takePicture().then(function(imageData) {
				$scope.car.imgData.push(imageData);
			});
		} else {
			$ionicPopup.alert({
				title: 'Liikaa kuvia',
				template: 'Voit lisätä yhdelle autolle maksimissaan viisi kuvaa.'
			});
		}
	}

	$scope.uploadPicture = function() {
		if($scope.car.imgData.length < 5) {
			Camera.uploadPicture().then(function(imageData) {
				$scope.car.imgData.push(imageData);
			});
		} else {
			$ionicPopup.alert({
				title: 'Liikaa kuvia',
				template: 'Voit lisätä yhdelle autolle maksimissaan viisi kuvaa.'
			});
		}
	}

	$scope.deletePicture = function(index) {
		$scope.car.imgData.splice(index, 1);
	}

	$scope.addCar = function(isValid) {
		if(isValid) {
			$ionicLoading.show({
				template: 'Lisätään autoa...'
			});

			if($scope.currentUser.address || $scope.currentUser.city || $scope.currentUser.postcode) {
				$scope.car.plate = $scope.car.plate.toUpperCase();
				$scope.car.brand = $scope.car.brand.name;

				Car.saveCar($scope.car).then(function(response) {
					$scope.car.imgData = [];	

					$ionicLoading.hide().then(function() {
						$ionicHistory.nextViewOptions({
							disableBack: true
						});

						$ionicHistory.clearCache(['app']).then(function() {
							$state.go('app.car', { id: response.data.car_id }, { reload: true });
						});
					});
				}, function(response) {
					$ionicLoading.hide();
				});				
			} else {
				$ionicLoading.hide().then(function() {
					$ionicPopup.alert({
						title: 'Tarkista osoite',
						template: 'Käyttäjäsi osoitetietoja ei löytynyt.'
					});
				});
			}
		}
	}

	$scope.fillData = function() {
		$scope.car.plate = 'AAA-000';
		$scope.car.brand = $scope.brands[0];
		$scope.getModels($scope.car.brand);
		$scope.car.model = $scope.models[0];
		$scope.car.year = $scope.years[0];
		$scope.car.gear = $scope.gears[0];
		$scope.car.fuel = $scope.fuels[0];
		$scope.car.kmPrice = 0.5;
		$scope.car.hPrice = 0.25;
		$scope.car.dPrice = 5;
		$scope.car.desc = 'Tämä on testiauto';
	}
})
