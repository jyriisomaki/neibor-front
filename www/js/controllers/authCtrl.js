angular.module('neibor').controller('authCtrl', function($scope, $state, $window, $ionicHistory, $ionicLoading, $ionicPopup, Auth, Map) {
	$scope.user = {};

	$scope.register = function() {
		if($scope.user.password === $scope.user.passwordAgain) {
			$ionicLoading.show({
				template: 'Rekisteröidään käyttäjää...'
			});

			Map.geocodeAddress($scope.user.address, $scope.user.city, $scope.user.postcode).then(function(response) {
				if(response.data.status == 'ZERO_RESULTS' || response.data.status == 'INVALID_REQUEST') {
					$ionicPopup.alert({
						title: 'Tarkista osoite',
						template: 'Osoitetta ei löytynyt.'
					});
				} else if(response.data.status == 'OVER_QUERY_LIMIT' || response.data.status == 'REQUEST_DENIED' || response.data.status == 'UNKNOWN_ERROR') {
					$ionicPopup.alert({
						title: 'Virhe',
						template: 'Jokin meni pieleen, yritä myöhemmin uudelleen.'
					});
				} else {
					$scope.user.coords = {
						lat: response.data.results[0].geometry.location.lat,
						lng: response.data.results[0].geometry.location.lng
					}

					Auth.register($scope.user).then(function(response) {
						$ionicHistory.nextViewOptions({
							disableBack: true
						});
		
						$state.go('app.login');
					}, function(response) {
						if(response.status == 403) {
							$ionicPopup.alert({
								title: 'Tarkista sähköposti',
								template: 'Tällä sähköpostilla on jo rekisteröity käyttäjä.'
							});							
						}
					});	
				}
			});
		} else {
			$ionicPopup.alert({
				title: 'Tarkista salasana',
				template: 'Salasanat eivät täsmää'
			});
		}

		$ionicLoading.hide();
	}

	$scope.login = function() {
		$ionicLoading.show({
			template: 'Kirjaudutaan sisään...'
		});

		Auth.login($scope.user).then(function() {
			$ionicLoading.hide();

			$ionicHistory.clearCache(['app']).then(function() {
				$state.go('app.home', {}, { reload: true });
			});
		}, function(response) {
			$ionicLoading.hide();

			if(response.status == 403 || response.status == 404) {
				$ionicPopup.alert({
					template: 'Väärä käyttäjätunnus tai salasana'
				});
			}
		});
	}

	$scope.logout = function() {
		$ionicLoading.show({
			template: 'Kirjaudutaan ulos...'
		});

		Auth.logout();
		$ionicLoading.hide();

		$ionicHistory.clearCache(['app']).then(function() {
			$state.go('app.home', {}, { reload: true });
		});
	}

	$scope.fillData = function() {
		$scope.user.email = 'jyri.isomaki@hotmail.com';
		$scope.user.password = 'test';
	}
});