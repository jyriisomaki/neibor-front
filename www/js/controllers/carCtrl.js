angular.module('neibor').controller('carCtrl', function($scope, $state, $stateParams, $ionicLoading, $ionicHistory, cars, Car, CAR_IMAGE_FOLDER) {
	$scope.CAR_IMAGE_FOLDER = CAR_IMAGE_FOLDER;
	$scope.car = cars;
	$scope. isUser = false;
	$scope.options = {
		loop: false,
		speed: 500,
	}

	if($scope.car.user._id === $scope.currentUser._id) {
		$scope.isUser = true;
	} else {
		$scope.isUser = false;
	}

	$scope.editCar = function() {
		$ionicLoading.show({
			template: 'Tallennetaan auton tietoja...'
		});

		Car.editCar($scope.car).then(function(response) {
			$ionicLoading.hide();
			
			$ionicHistory.clearCache().then(function() {
				$ionicLoading.hide();
				$state.go('^');	
			});
		}, function() {
			$ionicLoading.hide();
		});
	}

	$scope.$on("$ionicSlides.sliderInitialized", function(event, data){
  		$scope.slider = data.slider;
	});
});
