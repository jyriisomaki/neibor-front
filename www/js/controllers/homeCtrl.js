angular.module('neibor').controller('homeCtrl', function($scope, $ionicPopup, users, Map) {
	Map.getMap().then(function(map) {
		Map.addMarkersToMap(users, map);
	}, function(map) {
		$ionicPopup.alert({
			title: 'Virhe',
			template: 'Paikkatietojasi ei voitu hakea'
		});
		
		Map.addMarkersToMap(users, map);
	});
});