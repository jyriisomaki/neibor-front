angular.module('neibor').controller('neiborCtrl', function($rootScope, $scope, User, Auth, me) {
	$scope.currentUser = false;
	
	if(me) {
		$scope.currentUser = me.data;
	} else {
		$scope.currentUser = false;
	}
});
