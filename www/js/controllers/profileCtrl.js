angular.module('neibor').controller('profileCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $ionicPopup, Profile) {
	$scope.user = $scope.currentUser;
	$scope.review = {};
	$scope.review.stars = 0;
	
	$scope.editProfile = function() {
		$ionicLoading.show({
			template: 'Muokataan käyttäjän tietoja...'
		});

		if($scope.user.newPassword === $scope.user.newPasswordAgain) {
			Profile.editProfile($scope.user).then(function(response) {
				$ionicHistory.clearCache().then(function() {
					$ionicLoading.hide();
					$state.go('^');	
				});
			});
		} else {
			$ionicLoading.hide();

			$ionicPopup.alert({
				title: 'Tarkista salasana',
				template: 'Salasanat eivät täsmää'
			});
		}
	}
});
