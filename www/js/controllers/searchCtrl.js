angular.module('neibor').controller('searchCtrl', function($scope, User, Search) {
	$scope.filteredUsers = []

	$scope.searchByName = function(input) {
		$scope.filteredUsers = Search.searchByName(input);
	}

	$scope.searchByNumber = function(input) {
		$scope.filteredUsers = Search.searchByNumber(input);
	}
});
