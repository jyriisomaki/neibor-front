angular.module('neibor').controller('userCtrl', function($scope, $stateParams, $ionicPopup, user, User, PROFILE_IMAGE_FOLDER) {
	$scope.PROFILE_IMAGE_FOLDER = PROFILE_IMAGE_FOLDER;
	$scope.user = user;
	$scope.user.reviewsAvg = User.getReviewsAvg(user.reviews);
	$scope.review = {};
	$scope.review.stars = 0;
	
	$scope.addReview = function() {
		$ionicPopup.show({
			templateUrl: 'templates/review.html',
			cssClass: 'nbr-custom-popup',
			scope: $scope,
			title: 'Lisää arvostelu',
			buttons: [
				{ 
					text: 'Peruuta',
					onTap: function() {
						$scope.review = {};
						$scope.review.stars = 0;
					}
				},
				{ 
					text: 'Lisää',
					type: 'button-positive',
					onTap: function(e) {
						if($scope.review.stars <= 0) {
							document.getElementById('review-error').innerHTML = 'Arvostelulle on annettava ainakin yksi tähti';
							e.preventDefault();
						} else {
							User.saveReview($stateParams.id, $scope.review).then(function() {
								User.getUserById($stateParams.id).then(function(response) {
									$scope.user = response.data;
									$scope.user.reviewsAvg = User.getReviewsAvg($scope.user.reviews);
								});

								$scope.review = {};
								$scope.review.stars = 0;
							}, function(response) {
								if(response.status == 404) {
									$ionicPopup.alert({
										title: 'Virhe',
										template: 'Käyttäjää, jolle yrität lisätä arvostelua, ei löytynyt'
									});
								}
							});	
						}
					}
				}
			]
		});
	}
});
