angular.module('neibor').factory('Auth', function($http, API, AuthToken) {
	var service = {};

	service.register = function(user) {
		return $http.post(API + '/api/user/register', user, function(response) {
			return response;
		});
	}

	service.login = function(user) {
		return $http.post(API + '/api/user/login', user).then(function(response) {
			AuthToken.setToken(response.data.token);
		});
	}

	service.logout = function() {
		AuthToken.setToken();
	}

	service.verifyAuth = function() {
		if(AuthToken.getToken()) {
			return $http.get(API + '/api/profile/').then(function(response) {
				return response;
			}, function(response) {
				return false;
			});	
		} else {
			return false;
		}
	}

	return service;
});

angular.module('neibor').factory('AuthToken', function($window) {
	var service = {};

	service.setToken = function(token) {
		if(token) {
			$window.localStorage.setItem('token', token);	
		} else {
			$window.localStorage.removeItem('token');
		}
	}

	service.getToken = function() {
		return $window.localStorage.getItem('token');
	}

	return service;
});

angular.module('neibor').factory('AuthInterceptors', function(AuthToken) {
	var service = {};

	service.request = function(config) {
		var token = AuthToken.getToken();

		if(token) {
			config.headers['Authorization'] = token;
		}

		return config;
	}

	return service;
});