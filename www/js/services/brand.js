angular.module('neibor').factory('Brand', function($http, API) {
	var service = {};

	service.getBrands = function() {
		return $http.get(API + '/api/brand').then(function(response) {
			return response;
		});
	}

	return service;
});