angular.module('neibor').factory('Camera', function($cordovaCamera, $q) {
	var service = {};

	if(window.cordova) {
		var options = {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL
		}

		service.takePicture = function() {
			options.sourceType = Camera.PictureSourceType.CAMERA;

			return $cordovaCamera.getPicture(options).then(function(imageData) {
				return 'data:image/jpeg;base64,' + imageData;
			})
		}

		service.uploadPicture = function() {
			options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

			return $cordovaCamera.getPicture(options).then(function(imageData) {
				return 'data:image/jpeg;base64,' + imageData;
			});
		}		
	}

	return service;
})