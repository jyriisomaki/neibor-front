angular.module('neibor').factory('Car', function($http, API) {
	var service = {};

	service.getCars = function() {
		return $http.get(API + '/api/car').then(function(response) {
			return response;
		});
	}

	service.getCarById = function(id) {
		return $http.get(API + '/api/car/' + id).then(function(response) {
			return response;
		});
	}

	service.saveCar = function(car) {
		return $http.post(API + '/api/car', car).then(function(response) {
			return response;
		});
	}

	service.editCar = function(car) {
		return $http.put(API + '/api/car/' + car._id + '/edit', car).then(function(response) {
			return response;
		});
	}

	return service;
});