angular.module('neibor').factory('ErrorInterceptor', function($q, $injector) {
	return {
		responseError: function(response) {
			if(response.status === 500) {
				$injector.get('$ionicPopup').alert({
					title: 'Virhe',
					template: 'Jokin meni pieleen, yritä myöhemmin uudelleen.'
				});
			}

			return $q.reject(response);
		}
	}
});