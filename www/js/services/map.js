angular.module('neibor').factory('Map', function($http, $q, $cordovaGeolocation, $state, GOOGLE_API_KEY) {
	var service = {};
	var posOptions = {
		timeout: 10000,
		enableHighAccuracy: false
	}

	service.getMap = function() {
		var mapOptions = {
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true
		};

		return $q(function(resolve, reject) {
			if(window.cordova) {
				$cordovaGeolocation.getCurrentPosition(posOptions).then(function(pos) {
					mapOptions.center = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

					resolve(new google.maps.Map(document.getElementById('map'), mapOptions));
				}, function() {
					mapOptions.center = new google.maps.LatLng(62.255837, 25.750599); // Jyväskylä

					reject(new google.maps.Map(document.getElementById('map'), mapOptions));
				});
			} else {
				mapOptions.center = new google.maps.LatLng(62.255837, 25.750599); // Jyväskylä

				reject(new google.maps.Map(document.getElementById('map'), mapOptions));
			}			
		});
	}

	service.geocodeAddress = function(address, city, postcode) {
		var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + ',+ ' + city + ',+ ' + postcode + '&key=' + GOOGLE_API_KEY;

		return $http.get(url, function(response) {
			return response;
		});
	}

	service.addMarkersToMap = function(users, map) {
		for(var i = 0; i < users.length; i++) {
			if(users[i].cars.length && users[i].coords && users[i].coords != null) {
				var marker = new google.maps.Marker({
					map: map,
					position: new google.maps.LatLng(users[i].coords.lat, users[i].coords.lng),
					user: users[i]
				});

				google.maps.event.addListener(marker, 'click', function() {
					$state.go('app.profile', { id: this.user.id });
				});
			}
		}
	}

	return service;
});