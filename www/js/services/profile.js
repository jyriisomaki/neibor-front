angular.module('neibor').factory('Profile', function($http, API) {
	var service = {};

	service.editProfile = function(profile) {
		return $http.put(API + '/api/profile/', profile).then(function(response) {
			return response;
		});
	}

	service.getReviewsAvg = function(reviews) {
		var stars = 0;
		var count = 0;

		reviews.map(function(review) {
			stars += review.stars;
			count++;
		});

		return Math.round(stars / count);
	}

	return service;
});