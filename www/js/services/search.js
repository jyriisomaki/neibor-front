angular.module('neibor').factory('Search', function(User) {
	var service = {};
	var users = [];

	User.getUsers().then(function(response) {
		users = response.data;
	});

	service.searchByName = function(name) {
		var filteredUsers = [];

		if(name.length > 2) {
			name = name.replace(' ', '');
			name = name.replace('+', '');
			name = name.toLowerCase();

			users.map(function(user) {
				var fullname = user.firstname.toLowerCase() + user.lastname.toLowerCase();

				if(fullname.match(name) || name.includes(fullname)) {
					filteredUsers.push(user);
				}
			});
		}

		return filteredUsers;
	}

	service.searchByNumber = function(number) {
		var filteredUsers = [];
		number = number.substr(number.length - 9);

		users.map(function(user) {
			user.phone = user.phone.substr(user.phone.length -9);

			if(number == user.phone) {
				filteredUsers.push(user);
			}
		});

		return filteredUsers;
	}

	return service;
});