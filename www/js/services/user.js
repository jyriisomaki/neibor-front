angular.module('neibor').factory('User', function($http, Auth, API) {
	var service = {};

	service.getUsers = function() {
		return $http.get(API + '/api/user').then(function(response) {
			return response;
		});	
	}

	service.getUserById = function(id) {
		return $http.get(API + '/api/user/' + id).then(function(response) {
			return response;
		});
	}

	service.saveReview = function(userId, review) {
		return $http.put(API + '/api/user/' + userId, review, function(response) {
			return response;
		});
	}

	service.getReviewsAvg = function(reviews) {
		var stars = 0;
		var count = 0;

		reviews.map(function(review) {
			stars += review.stars;
			count++;
		});

		return Math.round(stars / count);
	}


	return service;
});